/*
Copyright © 2019 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	//"fmt"
	"log"
	"github.com/spf13/cobra"
	"html/template"
	"net/http"
)

// serviceCmd represents the service command
var serviceCmd = &cobra.Command{
	Use:   "service",
	Short: "Starts a web service for working with demo environments.",
	Long: `Starts a web service for working with demo environments.
Available endpoints are:
    / general home page`,
	Run: func(cmd *cobra.Command, args []string) {
		startService()
	},
}

func startService() {
	http.HandleFunc("/", homePage)
	log.Fatal(http.ListenAndServe(":8000", nil))
}

func homePage(w http.ResponseWriter, r *http.Request) {
	homepage.Execute(w, nil)
}

var homepage = template.Must(template.New("homepage").Parse(`
<!DOCTYPE html>
<html>
	<head>
		<style type='text/css' media='screen'>
			body { background-color: #6b4fbb;
			     	position: fixed;
					 	top: 50%;
					 	left: 50%;
					 	transform: translate(-50%, -50%);
					 	color: white;
					 	font-size: 250%; }
		</style>
  </head>
	<body>
		<p>Hello from GitLab!</p>
		<p><center>
			<img src='https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png' alt='Tanuki' width='250'>
		</center></p>
	</body>
</html>
`))

func init() {
	rootCmd.AddCommand(serviceCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// serviceCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// serviceCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
