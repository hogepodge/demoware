# Go Tool to Support Demo Creation

This is a simple go tool to assist in creating and managing demo environments.
Currently it features creating and deleting demo projects in GitLab.

# Building and Running

## Locally

```
go build
./demoware service
```

## Docker

```
docker build -t demoware .
docker run -p 8000:8000/tcp --rm demoware
```

# Available Commands

## service

Starts a web service for working with demo environments.
Available endpoints are:
```
  / : general home page
```  

## cloneproject

Clones a project from one source to a GitLab namespace, with
replicas if requested. Useful for taking a demo stub and preparing it for
running in front of a live studio audience.


### flags

```
--group-id int            The ID of the group to create the replica projects in
--project-source string   The source project to clone.
--project-target string   The target name of the project
--replicas int            The number of replicas to create. Will append ascending digits to the project name if > 1 (default 1)
--gitlab-token string   The application access token.
--gitlab-url string     The URL to the target GitLab instance. (default "https://gitlab.com")
```

## deleteproject

Deletes a project from a GitLab namespace, with replicas if requested. Assumes
the format used in clone-project.This is a destructive and dangerous command and
should be used with care!

### flags
```
--group-id int            The ID of the group to create the replica projects in
--project-target string   The target name of the project
--replicas int            The number of replicas to create. Will append ascending digits to the project name if > 1 (default 1)
--yes                     Yes, really delete the projects. Will dry run without this flag set
--gitlab-token string   The application access token.
--gitlab-url string     The URL to the target GitLab instance. (default "https://gitlab.com")
```
