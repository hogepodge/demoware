/*
Copyright © 2019 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"log"
	"github.com/xanzy/go-gitlab"
	"github.com/spf13/cobra"
)

var (
	yes bool
)

// deleteprojectCmd represents the deleteproject command
var deleteprojectCmd = &cobra.Command{
	Use:   "deleteproject",
	Short: "Deletes a set of demo projects. This action is destructive and should be used with care!",
	Long: `Deletes a project from a GitLab namespace, with replicas if requested. Assumes the format
used in clone-project.This is a destructive and dangerous command and should be used with care!`,
	Run: func(cmd *cobra.Command, args []string) {
		deleteProjects()
	},
}

func deleteProjects() {
	if replicas == 1 {
		deleteProject(projectTarget)
	} else {
		for i := 0; i < replicas; i++ {
			deleteProject(fmt.Sprintf("%s%d", projectTarget, i+1))
		}
	}
}

func deleteProject(name string) {
	remote := gitlab.NewClient(nil, gitlabToken)
	remote.SetBaseURL(gitlabURL)

	group, _, err := remote.Groups.GetGroup(groupID, nil)
	if err != nil {
		log.Fatal(err)
	}

	projects := group.Projects

	for _, project := range projects {
		if project.Name == name {
			fmt.Printf("Found project named %s in group %d\n", name, groupID)
			if yes {
				fmt.Printf("Deleting project %s\n\n", name)
				_, err = remote.Projects.DeleteProject(project.ID)
				if err != nil {
					fmt.Println("Delete failed", err)
				}
			} else {
				fmt.Printf("Specify --yes on the client to delete project %s\n\n", name)
			}
		}
	}
}

func init() {
	rootCmd.AddCommand(deleteprojectCmd)

	deleteprojectCmd.PersistentFlags().StringVar(&projectTarget, "project-target", "", "The target name of the project")
	deleteprojectCmd.MarkPersistentFlagRequired("project-target")

	deleteprojectCmd.PersistentFlags().IntVar(&replicas, "replicas", 1, "The number of replicas to create. Will append ascending digits to the project name if > 1")
	deleteprojectCmd.PersistentFlags().IntVar(&groupID, "group-id", 0, "The ID of the group to create the replica projects in")
	deleteprojectCmd.MarkPersistentFlagRequired("group-id")

	deleteprojectCmd.PersistentFlags().BoolVar(&yes, "yes", false, "Yes, really delete the projects. Will dry run without this flag set")

}
