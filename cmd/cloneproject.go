/*
Copyright © 2019 Chris Hoge/GitLab (choge@gitlab.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"log"
	"github.com/xanzy/go-gitlab"
	"github.com/spf13/cobra"
)

var (
	projectSource string
	projectTarget string
	replicas int
	groupID int
)

// cloneprojectCmd represents the cloneproject command
var cloneprojectCmd = &cobra.Command{
	Use:   "cloneproject",
	Short: "Clones a project from one source to a GitLab namespace, with replicas if requested.",
	Long: `Clones a project from one source to a GitLab namespace, with
replicas if requested. Useful for taking a demo stub and preparing it for running in front of a live studio audience.`,
	Run: func(cmd *cobra.Command, args []string) {
		cloneProject()
	},
}

func cloneProject() {
	if replicas == 1 {
		createProject(projectTarget)
	} else {
		for i := 0; i < replicas; i++ {
			createProject(fmt.Sprintf("%s%d", projectTarget, i+1))
		}
	}
}

func createProject(name string) {
	remote := gitlab.NewClient(nil, gitlabToken)
	remote.SetBaseURL(gitlabURL)

	public := gitlab.PublicVisibility

	popts := &gitlab.CreateProjectOptions{
		Name: gitlab.String(name),
		Description: gitlab.String("A GitLab Project"),
		ImportURL: gitlab.String(projectSource),
		NamespaceID: gitlab.Int(groupID),
		Visibility: &public,
	}

	_, _, err := remote.Projects.CreateProject(popts)
	if err != nil {
		log.Fatal(err)
	}
}

func init() {
	rootCmd.AddCommand(cloneprojectCmd)

	cloneprojectCmd.PersistentFlags().StringVar(&projectSource, "project-source", "", "The source project to clone.")
	cloneprojectCmd.MarkPersistentFlagRequired("project-source")

	cloneprojectCmd.PersistentFlags().StringVar(&projectTarget, "project-target", "", "The target name of the project")
	cloneprojectCmd.MarkPersistentFlagRequired("project-target")

	cloneprojectCmd.PersistentFlags().IntVar(&replicas, "replicas", 1, "The number of replicas to create. Will append ascending digits to the project name if > 1")
	cloneprojectCmd.PersistentFlags().IntVar(&groupID, "group-id", 0, "The ID of the group to create the replica projects in")
	cloneprojectCmd.MarkPersistentFlagRequired("group-id")

}
