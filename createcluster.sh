eksctl create cluster \
  --name lighthouse \
  --version 1.14 \
  --nodegroup-name standard-workers \
  --node-type t3.medium \
  --nodes 6 \
  --nodes-min 4 \
  --nodes-max 8 \
  --node-ami auto
