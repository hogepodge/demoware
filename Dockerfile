FROM golang:alpine as builder
RUN mkdir -p /go/src/gitlab.com/hogepodge/demoware
ADD . /go/src/gitlab.com/hogepodge/demoware
WORKDIR /go/src/gitlab.com/hogepodge/demoware
RUN apk add --no-cache git
RUN go get
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o demoware .

FROM alpine
EXPOSE 8000
RUN adduser -S -D -H -h /app gitlab
USER gitlab
COPY --from=builder /go/src/gitlab.com/hogepodge/demoware/demoware /app/demoware
WORKDIR /app
CMD ["./demoware", "service"]
